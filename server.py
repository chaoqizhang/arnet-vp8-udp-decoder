import os
import redis
import cv2
import imutils
import math
import numpy as np

VpxFrameKey = "udp_vpx_frames"

r = redis.Redis('localhost', 6379)
frame_len = r.llen(VpxFrameKey)

frame_width = 320
frame_height = 240

face_cascade = cv2.CascadeClassifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml")

for i in range(0, frame_len):
    frameBuf = r.lindex(VpxFrameKey, i)
    
    frame = np.zeros(frame_width, dtype=np.uint8)
    for h in range(0, int(frame_height/2*3)):
        frameTmp = np.frombuffer(frameBuf, dtype=np.uint8, count = frame_width, offset = frame_width * h)
        frame = np.row_stack((frame, frameTmp))
    frame = np.delete(frame, 0, 0)
    gray = cv2.cvtColor(frame, cv2.COLOR_YUV2GRAY_420)
    bgr_frame = cv2.cvtColor(frame, cv2.COLOR_YUV2BGR_I420)
    bgr_frame_rotated = bgr_frame
    face_reg_flag = False
    for angle in np.arange(0, 360, 90):
        gray_rotated = imutils.rotate_bound(gray, angle)
        faces = face_cascade.detectMultiScale(gray_rotated, 1.1, 4)
        if type(faces) == tuple:
            continue
        else:
            face_reg_flag = True
            print(angle)
            for(x,y,w,h) in faces:

                # frame_center = (int(frame_width/2), int(frame_height/2))
                # center = (int(x+w/2), int(y+h/2))
                # x1 = center[0]
                # y1 = frame_height - center[1]
                # x2 = frame_center[0]
                # y2 = frame_height - frame_center[1]
                # theta = (360-angle)/180*math.pi
                # rotated_center_x = (x1-x2)*math.cos(theta)-(y1-y2)*math.sin(theta)+x2
                # rotated_center_y = (x1-x2)*math.sin(theta)+(y1-y2)*math.cos(theta)+y2
                # rotated_center = (int(rotated_center_x), int(rotated_center_y))
                # radius = int(0.5*(math.sqrt(math.pow(w,2) + math.pow(h,2))))
                # cv2.circle(bgr_frame, rotated_center, radius, (255, 0, 0), 2)
                bgr_frame_rotated = imutils.rotate_bound(bgr_frame, angle)
                bgr_frame_rotated = cv2.rectangle(bgr_frame_rotated, (x,y), (x+w,y+h), (255,0,0), 2)
                bgr_frame_rotated = imutils.rotate_bound(bgr_frame_rotated, (360-angle))
                # cv2.rectangle(bgr_frame, (x,y), (x+w,y+h), (255,0,0), 2)
            break
       
    cv2.imshow("frame", bgr_frame_rotated)
    cv2.waitKey(10)

cv2.destroyAllWindows()
